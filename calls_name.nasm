calls_name:
dw 	31
db 	""					, EOS
db	"history"			, EOS
db	"cls"				, EOS
db	"bgcolor"			, EOS
db	"textcolor"			, EOS
db	"time"				, EOS
db	"date"				, EOS
db	"help"				, EOS
db	"reboot"			, EOS
db	"shutdown"			, EOS
db	"lspci"				, EOS
db	"edit"				, EOS
db	"ram"				, EOS
db	"version"			, EOS
db	"lock"				, EOS
db	"password"			, EOS
db	"calc"				, EOS
db	"whoami"			, EOS
db	"sleep"				, EOS
db	"onlinetime"		, EOS
db	"uname"				, EOS
db	"cursor"			, EOS
db	"cal"				, EOS
db	"separator"			, EOS
db	"screenlines"		, EOS
db 	"cpuid"				, EOS
db 	"draw"				, EOS
db 	"suspend"			, EOS
db 	"echo"				, EOS
db 	"rom"				, EOS
db 	"ramsector"			, EOS	; exemplu de apel: `ramsector 1984 0`, unde 1984 (0x07c0 in hex) reprezinta adresa
								; de segment, iar 0 - este adresa de offset
								; astfel, acest apel va afisa pe ecran bootloaderul nostru (:
