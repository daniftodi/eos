_cursor_essi_:
	push ax
	push si
	push fs
	push di
	push cx

	mov di, cs
	mov fs, di
	mov cx, 1
	call lgeti_essicx_di
	mov si, di

	mov dx, _nullfunction__

	mov di, .str1
	call siisequal_essifsdi_ah
	cmp ah, FALSE
	je .skip

	mov dx, dcursorshow__
	jmp .end

	.skip:
	mov di, .str2
	call siisequal_essifsdi_ah
	cmp ah, FALSE
	je .end

	mov dx, dcursorhide__

	.end:

	call dx

	pop cx
	pop di
	pop fs
	pop si
	pop ax
	ret
		.str1 db "on", EOS
		.str2 db "off", EOS