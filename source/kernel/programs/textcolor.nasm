_textcolor_essi_:
	push es
	push fs
	push si
	push di
	push ax
	push cx

	mov di, es
	mov fs, di
	mov cx, 1
	call lgeti_essicx_di

	mov si, cs
	mov es, si
	mov si, COLORS
	call licontains_essifsdi_cxah

	cmp ah, FALSE
	je .skip

	mov ah, [display.attribute]
	and ah, 0xf0
	and cl, 0x0f
	or ah, cl
	call dsetcolor_ah_

	.skip:

	pop cx
	pop ax
	pop di
	pop si
	pop fs
	pop es
	ret