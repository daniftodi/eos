_separator_essi_:
	push cx
	push ax
	push dx

	mov cl, [display.columns]
	xor ch, ch
	mov dx, dprintchar_al_
	mov al, HLINE

	call _repeat_dxcx_

	pop dx
	pop ax
	pop cx
	ret