_lock_essi_:
	pusha

	mov si, cs
	mov es, si
	mov fs, si

	mov cx, PASSWORD_LEN
	mov dx, cisprint_al_ah
	mov di, _password_essi_.password2
	.again:
		mov si, .str1
		call dprints_essi_

		mov si, _password_essi_.password1

		call kreadhideline_esdidxcx_
		call sisequal_essifsdi_ah
		cmp ah, TRUE
		je .skip

		call dnewline__

		mov si, .str2
		call dprintsln_essi_
		jmp .again
	.skip:

	call dnewline__

	popa
	ret
		.str1 db "Terminal is locked. Please insert password for deblock: ", EOS
		.str2 db "Incorect password!!!", EOS
