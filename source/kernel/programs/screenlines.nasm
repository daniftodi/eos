_screenlines_essi_:
	push ax
	push cx
	push dx
	push di

	mov cx, 1
	call lgeti_essicx_di

	xchg si, di
	call stonu16_essi_ax

	mov si, di

	mov dx, dsett80x50__
	mov di, dscreencontenttostackup__

	cmp ax, 50
	je .end

	mov dx, dsett80x25__
	cmp ax, 25
	je .end

	mov dx, _nullfunction__
	mov di, dx

	.end:

	call di
	call dx

	pop di
	pop dx
	pop cx
	pop ax
	ret