_ram_essi_:
	push ax
	push bx
	push dx

	mov ax,0xe801
	int 0x15

	mov ax,dx
	mov bx,16
	xor dx,dx
	div bx

	add ax,bx
	call dprintnu16_ax_

	call dnewline__

	pop dx
	pop bx
	pop ax
	ret
