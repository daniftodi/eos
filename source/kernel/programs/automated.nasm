arepeat_alahcldx_:
	push ax
	add ah, cl
	.again:
		cmp al, ah
		je ._again

		call dx

		add ah, cl
		jmp .again
	._again:
	pop ax
	ret


;*************************************************************************
; local functions

;local_repeat_alahcl
