_onlinetime_essi_:
	push ax
	push cx
	push dx

	call dcursorhide__

	.again:

		mov ah, 0x02
		int 0x1a

		mov al, ch
		call dprintBCD_al_

		mov al, ':'
		call dprintchar_al_

		mov al, cl
		call dprintBCD_al_

		mov al, ':'
		call dprintchar_al_

		mov al, dh
		call dprintBCD_al_

		mov ah, 0x86
		mov cx, 0x000e
		mov dx, 0xffff
		int 0x15		

		mov dx, dlbackspace__
		mov cx, 0x0008
		call _repeat_dxcx_

		mov ah, 0x01
		int 0x16

		jz .again

	call dcursorshow__

	call kgetch__

	pop dx
	pop cx
	pop ax
	ret

