_time_essi_:
	push ax
	push cx
	push dx

	mov ah, 0x02
	int 0x1a

	mov al, ch
	call dprintBCD_al_

	mov al, ':'
	call dprintchar_al_

	mov al, cl
	call dprintBCD_al_

	mov al, ':'
	call dprintchar_al_

	mov al, dh
	call dprintBCD_al_

	call dnewline__

	pop dx
	pop cx
	pop ax
	ret

