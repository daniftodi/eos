_ramsector_essi_:
	push es
	push si
	push di
	push ax
	push cx

	mov cx, 2
	call lgeti_essicx_di
	xchg si, di
	call stonu16_essi_ax
	xchg si, di

	dec cx
	call lgeti_essicx_di
	mov si, di
	mov cx, ax
	call stonu16_essi_ax

	mov es, ax
	mov si, cx

	;mov cx, 512
	;call _showramcontent_essicx_
	;call dnewline__

	mov al, ' '
	call dprintchar_al_
	call dprintchar_al_
	call dprintchar_al_
	call dprintchar_al_
	call dprintchar_al_

	mov al, VLINE
	call dprintchar_al_

	mov cx, 16
	mov ah, 0
	mov al, ' '

	.antet:
		call dprintchar_al_
		xchg al, ah
		inc al
		call dprinthex_al_
		xchg al, ah
		loop .antet

	call dnewline__
	mov al, HLINE
	call dprintchar_al_
	call dprintchar_al_
	call dprintchar_al_
	call dprintchar_al_
	call dprintchar_al_

	mov al, CENTER
	call dprintchar_al_

	mov cx, 48
	mov dx, dprintchar_al_
	mov al, HLINE
	call _repeat_dxcx_

	mov ax, 0x0000

	jmp .skip

	.lines:
		add ax, 0x0010
		.skip:
		push ax

		call dnewline__
		xchg al, ah
		call dprinthex_al_
		xchg al, ah
		call dprinthex_al_
		mov al, ' '
		call dprintchar_al_
		mov al, VLINE
		call dprintchar_al_
		mov cx, 16
		.line:
			mov al, ' '
			call dprintchar_al_
			mov al, [es:si]
			call dprinthex_al_
			inc si
			loop .line

		pop ax
		cmp ax, 0x01f0
		jne .lines

	call dnewline__
	mov al, HLINE
	call dprintchar_al_
	call dprintchar_al_
	call dprintchar_al_
	call dprintchar_al_
	call dprintchar_al_
	mov al, CENTERB
	call dprintchar_al_

	mov cx, 48
	mov dx, dprintchar_al_
	mov al, HLINE
	call _repeat_dxcx_
	call dnewline__

	pop cx
	pop ax
	pop di
	pop si
	pop es
	ret

_showramcontent_essicx_:
	push si
	push cx
	push ax

	.again:
		mov al, [es:si]
		call dprinthex_al_
		mov al, ' '
		call dprintchar_al_

		inc si
		loop .again

	pop ax
	pop cx
	pop si
	ret

