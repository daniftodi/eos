draw:
	.segment 	dw 	0xa000
	.bgcolor 	db 	0xff

_draw_essi_:
	push gs
	push si
	push ax

	mov si, [draw.segment]
	mov gs, [si]

	call dsetg640x480__

	mov ax, 0xf0f0

	mov [gs:0x0], ax

	pop ax
	pop si
	pop gs

	jmp $

	ret