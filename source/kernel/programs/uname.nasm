_uname_essi_:
	push es
	push si
	push di
	push cx
	push fs

	mov cx, [es:si]
	cmp cx, 1
	je .skip

	mov cx, 1
	call lgeti_essicx_di
	mov si, di

	mov di, cs
	mov fs, di
	mov di, _whoami_essi_.str

	call scopy_essifsdi_
	jmp .end

	.skip:

	mov si,cs
	mov es,si
	mov si,.str

	call dprints_essi_

	mov di, _whoami_essi_.str
	mov cx, USERNAME_LEN
	mov dx, cisprint_al_ah
	call kreadline_esdidxcx_
	call dnewline__

	.end:
	pop fs
	pop cx
	pop di
	pop si
	pop es
	ret
		.str db "Enter your new name: ", EOS

