_cpuid_essi_:
	pusha

	mov eax, 0x80000000
	cpuid

	mov eax, ebx
	mov cx, 4
	.again1:
		call dprintchar_al_
		ror eax, 8
		loop .again1

	mov eax, edx
	mov cx, 4
	.again2:
		call dprintchar_al_
		ror eax, 8
		loop .again2

	mov eax, ecx
	mov cx, 4
	.again3:
		call dprintchar_al_
		ror eax, 8
		loop .again3

	call kgetch__
	call dnewline__

	popa
	ret