_password_essi_:
	pusha

	mov si, cs
	mov es, si
	mov fs, si

	mov ah, TRUE
	mov dx, cisprint_al_ah
	mov cx, PASSWORD_LEN

	mov si, .password1
	mov al, [es:si]
	cmp al, EOS
	je .skip

	mov si, .str1
	call dprints_essi_

	mov di, .password2
	mov si, .password1

	call kreadhideline_esdidxcx_
	call sisequal_essifsdi_ah
	call dnewline__

	.skip:
	mov si, .str2
	call dprints_essi_
	mov di, .password2
	call kreadhideline_esdidxcx_

	call dnewline__

	mov si, .str3
	call dprints_essi_
	mov di, .password3
	call kreadhideline_esdidxcx_

	call dnewline__

	cmp ah, TRUE
	je .skip2

	mov si, .str4
	call dprints_essi_
	jmp .end

	.skip2:
	mov si, .password2
	call sisequal_essifsdi_ah
	cmp ah, TRUE
	je .skip3

	mov si, .str5
	call dprints_essi_
	jmp .end

	.skip3:
	mov si, di
	mov di, .password1
	call scopy_essifsdi_
	mov si, .str6
	call dprints_essi_

	.end:
	call dnewline__

	mov ax, 1
	call _sleep_ax_

	popa
	ret
		.str1 db "Enter your old password: ", EOS
		.str2 db "Enter new password: ", EOS
		.str3 db "Reenter new password: ", EOS
		.str4 db "Error! Not correct old password", EOS
		.str5 db "Error! Not coincide new password", EOS
		.str6 db "Succes. Password is changed", EOS
		.password1 times (PASSWORD_LEN + 1) db 0
		.password2 times (PASSWORD_LEN + 1) db 0
		.password3 times (PASSWORD_LEN + 1) db 0