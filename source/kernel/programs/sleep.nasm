_sleep_essi_:
	push ax
	push cx
	push di

	mov cx, 1
	call lgeti_essicx_di

	xchg si, di

	call stonu16_essi_ax
	
	mov si, di

	pop di
	pop cx

	call _sleep_ax_

	pop ax
	ret

_sleep_ax_:
	push ax
	push cx
	push dx
	push di

	mov di, ax

	mov ah, 0x86
	mov cx, 0x000f
	mov dx, 0x4240

	jmp .skip
	.again:
		int 0x15
		dec di
		.skip:
		test di, di
		jnz .again

	pop di
	pop dx
	pop cx
	pop ax
	ret