_lspci_essi_:
	push es
	push si

	mov si,cs
	mov es,si
	
	_start:
		mov si,.startMsg_str
		call dprintsln_essi_
	
	int 0x11
	
	cmp ax,1
	je .math
	
	shl ax,1
	cmp ax,1
	je .ps2
	
	;Reserved bit
	shl ax,1
	
	shl ax,1
	mov ax,bx
	and bx,3
	cmp bx,3
	je .vm3
	
	cmp bx,2
	je .vm2
	
	shl bx,1
	cmp bx,1
	je .vm1
	
	cmp bx,0
	je .vm0
	
	jmp .end
 	
	.math:
		mov si,.math_str
		call dprintsln_essi_
	.ps2:
		mov si,.ps2_str
		call dprintsln_essi_
	.vm3:
		mov si,.vm3_str
		call dprintsln_essi_
	.vm2:
		mov si,.vm2_str
		call dprintsln_essi_
	.vm1:
		mov si,.vm1_str
		call dprintsln_essi_
	.vm0:
		mov si,.vm0_str
		call dprintsln_essi_

	; Partea a doua
	mov ah,0x43
	int 0x15
	
	.lcd:
		cmp al,1
		jne .rs232
		mov si,.lcd_str
		call dprintsln_essi_
	.rs232:
		shl al,1
		cmp al,1
		jne .modem
		mov si,.rs232_str
		call dprintsln_essi_
	
	.modem:
		shl al,1
		shl al,1 ; 1 bit reserved
		cmp al,1
		jne .extPow
		mov si,.modem_str
		call dprintsln_essi_
	.extPow:	
		shl al,1
		cmp al,1
		jne .battery
		mov si,.extPow_str
		call dprintsln_essi_
		
	.battery:
		shl al,1
		jne .end
		mov si,.battery_str
		call dprintsln_essi_
	
	; End partea a doua
	.end:
		mov si,.endMsg_str
		call dprintsln_essi_
	pop si
	pop es
	ret
		.startMsg_str db "Start detecting devices",EOS
		.math_str db "Math coprocessor installed",EOS
		.ps2_str db "PS/2 mouse is installed",EOS
		.vm0_str db "EGA,VGA,PGA, or other with on-board video BIOS",EOS
		.vm1_str db "40x25 CGA color.",EOS
		.vm2_str db "80x25 CGA color (emulator default).",EOS
		.vm3_str db "80x25 mono text.",EOS
		.endMsg_str db "End detecting devies",EOS
		.lcd_str db "LCD detached",EOS
		.rs232_str db "RS232/parallel powered on",EOS
		.modem_str db "internal modem powered on",EOS
		.extPow_str db "external power in use",EOS
		.battery_str db "battery low",EOS