_date_essi_:
	push ax
	push cx
	push dx

	mov ah, 0x04
	int 0x1a

	mov al, dl
	call dprintBCD_al_
	mov al, '.'
	call dprintchar_al_

	mov al, dh
	call dprintBCD_al_
	mov al, '.'
	call dprintchar_al_

	mov ax, cx
	call dprintBCD_ax_

	call dnewline__

	pop dx
	pop cx
	pop ax
	ret
