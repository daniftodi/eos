_edit_essi_:
	push di
	push cx
	push ax

	mov ax, [es:si]
	xor cx, cx

	call dprintnu16_ax_

	call dnewline__

	.again:
		dec ax
		call lgeti_essicx_di
		xchg si, di
		call dprintsln_essi_
		xchg si, di
		inc cx
		test ax, ax
		jnz .again

	pop ax
	pop cx
	pop di
	ret
