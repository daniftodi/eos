history:
	.segment dw HISTORY_SEGMENT
	.bp dw history_buffer
	.sp dw history_buffer
	.from dw history_buffer
	.to dw (history_buffer + HISTORY_SIZE * 2)

_history_essi_:
	push es
	push si
	push di
	push fs

	mov si, cs
	mov es, si

	mov si, [history.segment]
	mov fs, si

	mov di, [history.sp]

	.again:
		cmp di, [history.bp]
		je ._again

		mov si, [fs:di]
		call dprintsln_essi_

		cmp di, [history.from]
		jne .skip
		mov di, [history.to]

		.skip:
		sub di, 2
		jmp .again

	._again:

	pop fs
	pop di
	pop si
	pop es
	ret

hpush_di_:
	push si
	push es

	mov si, [history.segment]
	mov es, si

	mov si, [history.sp]

	cmp di, [es:si]
	je .end

	call hnextsp__

	mov si, [history.segment]
	mov es, si
	mov si, [history.sp]

	mov [es:si], di
	
	.end:
	pop es
	pop si
	ret
		.str1 db "push la ", EOS
		.str2 db ""

hnextbp__:
	push si

	mov si, [history.bp]
	add si, 2
	cmp si, [history.to]
	jne .skip

	mov si, [history.from]

	.skip:
	mov [history.bp], si

	pop si
	ret

hnextsp__:
	push si

	mov si, [history.sp]
	add si, 2
	cmp si, [history.to]
	jne .skip

	mov si, [history.from]

	.skip:
	mov [history.sp], si
	cmp si, [history.bp]
	jne .end

	call hnextbp__

	.end:
	pop si
	ret

history_buffer:
times HISTORY_SIZE dw 0