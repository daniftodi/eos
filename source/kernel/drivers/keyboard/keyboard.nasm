kgetch__alah:
	xor ah, ah
	int 0x16
	ret

kgetch__:
	push ax
	call kgetch__alah
	pop ax
	ret

kreadline_esdidxcx_:
	push ax
	push bx
	push cx
	push si
	push di

	mov si, cx

	xor cx, cx
	mov bx, cx

	.again:
		call kgetch__alah

		cmp ah, SCAN_KEY_UP
		je .SCROLLUP

		cmp ah, SCAN_KEY_DOWN
		je .SCROLLDN

		push ax
		push cx
		push dx

		.tocursor:
			call dstackdnisempty__ah
			cmp ah, TRUE
			je ._tocursor

			call dscrolldn__
			jmp .tocursor

		._tocursor:
		
		pop dx
		pop cx
		pop ax

		cmp ah, SCAN_KEY_LEFT
		je .LEFT

		cmp ah, SCAN_KEY_RIGHT
		je .RIGHT

		cmp ah, SCAN_KEY_BACKSPACE
		je .BACKSPACE				; left backspace

		cmp ah, SCAN_KEY_DELETE
		je .DELETE

		cmp ah, SCAN_KEY_ESC
		je .ESC

		cmp al, NEWLINE
		je .NEWLINE

		call dx

		cmp ah, FALSE
		je .again

		cmp cx, si
		je .again

		inc cx
		inc bx

		call dprintchar_al_
		call dstackdnisempty__ah
		cmp ah, TRUE
		je .again

		call dscrolldn__
		jmp .again

		.LEFT:
			test bx, bx
			jz .again
			dec bx
			call dbackcursorposition__
			jmp .again

		.RIGHT:
			cmp bx, cx
			je .again
			inc bx
			call dnextcursorposition__
			jmp .again

		.SCROLLUP:
			call dscrollup__
			jmp .again

		.SCROLLDN:
			call dscrolldn__
			jmp .again

		.BACKSPACE:
			test bx, bx
			jz .again
			dec bx
			dec cx
			call dlbackspace__
			jmp .again

		.DELETE:
			cmp bx, cx
			je .again
			dec cx
			call ddelete__
			jmp .again

		.ESC:
			mov ax, [display.cursor_position]
			sub ax, bx
			mov [display.cursor_position], ax
			add ax, cx

			mov di, ax
			shl di, 1
			mov al, EOS
			inc cx
			.esc_again:
				mov [gs:di], al
				sub di, 2
				loop .esc_again
			call dsetcursorposition__
			mov bx, cx

			jmp .again

		.NEWLINE:
			pop di
			mov si, [display.cursor_position]
			sub si, bx
			shl si, 1

			call ktakelinefromdisplay_gssiesdi_

			shr si, 1
			add si, cx
			mov [display.cursor_position], si

	pop si
	pop cx
	pop bx
	pop ax
	ret

ktakelinefromdisplay_gssiesdi_:
	push si
	push di
	push ax

	.again:
		mov al, [gs:si]
		mov [es:di], al

		inc di
		add si, 2

		cmp al, EOS
		jne .again

	pop ax
	pop di
	pop si
	ret

kreadhideline_esdidxcx_:
	push bx

	mov bh, [display.attribute]

	and bh, 0xf0
	mov bl, bh
	shr bl, 4
	or bl, bh

	mov bh, [display.attribute]
	mov [display.attribute], bl

	call dcursorhide__
	call kreadline_esdidxcx_

	push cx
	push dx
	xchg si, di

	mov dx, dlbackspace__
	call slength_essi_cx
	call _repeat_dxcx_

	mov [display.attribute], bh
	call dcursorshow__

	xchg si, di
	pop dx
	pop cx
	pop bx
	ret

