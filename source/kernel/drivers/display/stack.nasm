stack:
	.segment	dw 	0
	.spup		dw 	0
	.spdn		dw 	0

dstackset__:
	push ax

	xor ax, ax
	mov [stack.spup], ax

	not ax
	mov [stack.spdn], ax

	mov ax, SGSEGMENT
	mov [stack.segment], ax

	pop ax
	ret

dstackupisempty__ah:
	mov ah, FALSE

	push bx
	mov bx, [stack.spup]
	test bx, bx
	jnz .skip
	mov ah, TRUE
	.skip:
	pop bx
	ret

dstackuppush_al_:
	cmp al, EOS
	je .end

	push es
	push si

	mov si, [stack.segment]
	mov es, si

	mov si, [stack.spup]
	inc si

	cmp si, [stack.spdn]
	je .skip

	mov [es:si], al
	mov [stack.spup], si

	.skip:
	pop si
	pop es

	.end:
	ret

dstackuppop__al:
	push es
	push si

	mov al, EOS

	mov si, [stack.segment]
	mov es, si

	mov si, [stack.spup]
	test si, si
	jz .skip

	mov al, [es:si]
	dec si
	mov [stack.spup], si

	.skip:
	pop si
	pop es
	ret

dstackdnisempty__ah:
	mov ah, FALSE

	push bx
	mov bx, [stack.spdn]
	cmp bx, 0xffff
	jne .skip
	mov ah, TRUE
	.skip:
	pop bx
	ret

dstackdnpush_al_:
	cmp al, EOS
	je .end

	push es
	push si

	mov si, [stack.segment]
	mov es, si

	mov si, [stack.spdn]
	dec si

	cmp si, [stack.spup]
	je .skip

	mov [es:si], al
	mov [stack.spdn], si

	.skip:
	pop si
	pop es
	.end:
	ret

dstackdnpop__al:
	push es
	push si

	mov al, EOS

	mov si, [stack.segment]
	mov es, si

	mov si, [stack.spdn]
	cmp si, 0xffff
	je .skip

	mov al, [es:si]
	inc si
	mov [stack.spdn], si

	.skip:
	pop si
	pop es
	ret

dstackclear__:
	call dstackupclear__
	call dstackdnclear__
	ret

dstackupclear__:
	push si

	xor si, si
	mov [stack.spup], si

	pop si
	ret

dstackdnclear__:
	push si

	xor si, si
	not si

	mov [stack.spdn], si

	pop si
	ret