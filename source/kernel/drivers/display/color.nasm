COLORS:
dw 	16
db 	"BLACK"			, EOS
db 	"BLUE"			, EOS
db 	"GREEN"			, EOS
db 	"CYAN"			, EOS
db 	"RED"			, EOS
db 	"MAGENTA"		, EOS
db 	"BROWN"			, EOS
db 	"LIGHTGRAY"		, EOS
db 	"DARKGRAY"		, EOS
db 	"LIGHTBLUE"		, EOS
db 	"LIGHTGREEN"	, EOS
db 	"LIGHTCYAN"		, EOS
db 	"LIGHTRED"		, EOS
db 	"LIGHTMAGENTA"	, EOS
db 	"YELLOW"		, EOS
db 	"WHITE"			, EOS
