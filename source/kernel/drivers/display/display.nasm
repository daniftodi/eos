buffer8:		dd	0x0, 0x0

display:
	.cursor_position			dw 	0
	.max_cursor_position		dw 	0
	.columns					db	0
	.lines						db 	0
	.attribute					db 	0

HEX: db "0123456789ABCDEF"
hex: db "0123456789abcdef"

;**********************************************************************************************
; display
;**********************************************************************************************
dsetg640x480__:
	push ax

	mov ax, 0x0012
	int 0x10

	pop ax
	ret

dsettresolution_alah_:
	push bx

	mov [display.lines], al
	mov [display.columns], ah

	mov bx, ax
	mul bh
	dec ax

	mov [display.max_cursor_position], ax

	mov ax, bx
	pop bx
	ret

dsett80x25__:
	push ax

	mov al, 25
	mov ah, 80
	call dsettresolution_alah_

	mov ax, 0x03
	int 0x10

	xor ax, ax
	mov [display.cursor_position], ax

	;call dstackset__
	call dcls__

	pop ax

	ret

dsett80x50__:
	push ax
	push bx

	mov al, 50
	mov ah, 80
	call dsettresolution_alah_

	mov ax, 0x1112
	xor bx, bx
	int 0x10

	xor ax, ax
	mov [display.cursor_position], ax

	;call dstackset__
	call dcls__

	pop bx
	pop ax
	ret

dcls__:
	mov [.tmp], ah

	mov ah, [display.attribute]
	call dcls_ah_

	mov ah, [.tmp]
	ret
		.tmp db 0

dcls_ah_:
	push si
	push ax

	mov al, EOS
	mov si, [display.max_cursor_position]
	inc si
	shl si, 1

	.again:
		sub si, 2
		mov [gs:si], ax
		test si, si
		jnz .again

	xor ax, ax
	mov [display.cursor_position], ax
	call dsetcursorposition__

	pop ax
	pop si

	mov [display.attribute], ah

	ret

dupwithoneline__:
	push si
	push di
	push ax
	push cx

	xor di, di
	
	xor ah, ah
	mov al, [display.columns]
	shl ax, 1
	mov si, ax

	mov cx, [display.max_cursor_position]
	shl cx, 1

	jmp .skip

	.again:
		add si, 2
		add di, 2
		.skip:
		mov ax, [gs:si]
		mov [gs:di], ax

		cmp si, cx
		jne .again

	pop cx
	pop ax
	pop di
	pop si
	ret

ddownwithoneline__:
	push si
	push di
	push ax
	push cx

	mov cl, [display.columns]
	xor ch, ch

	mov di, [display.max_cursor_position]

	mov si, di
	sub si, cx

	shl di, 1
	shl si, 1

	jmp .skip

	.again:
		sub si, 2
		sub di, 2
		.skip:
		mov ax, [gs:si]
		mov [gs:di], ax

		test si, si
		jnz .again

	pop cx
	pop ax
	pop di
	pop si
	ret

dsetcolor_ah_:
	push di

	mov [display.attribute], ah
	mov di, [display.max_cursor_position]

	inc di

	.again:
		dec di
		call dreputchar_di_
		test di, di
		jnz .again

	pop di
	ret

ddelcontent__:
	call dstackclear__
	call dcls__
	ret

dscreencontenttostackup__:
	push ax

	xor al, al
	mov ah, [display.lines]

	.again:
		call donelinetostackup_al_
		inc al

		cmp al, ah
		jne .again

	call dcls__
	xor ax, ax
	mov [display.cursor_position], ax
	call dsetcursorposition__

	pop ax

	ret


;**********************************************************************************************
; numbers
;**********************************************************************************************
dprintnu8_al_:
	push dx
	mov dx, nu8tostr_alesdi_
	call dprintn_dx_
	pop dx
	ret

dprintns8_al_:
	push dx
	mov dx, ns8tostr_alesdi_
	call dprintn_dx_
	pop dx
	ret

dprintnu16_ax_:
	push dx
	mov dx, nu16tostr_axesdi_
	call dprintn_dx_
	pop dx
	ret

dprintns16_ax_:
	push dx
	mov dx, ns16tostr_axesdi_
	call dprintn_dx_
	pop dx
	ret

dprintn_dx_:
	push es
	push di

	mov di, cs
	mov es, di

	mov di, buffer8
	call dx

	xchg si, di

	call dprints_essi_

	mov si, di

	pop di
	pop es
	ret

dprintBCD_al_:
	push ax

	mov ah, al
	shr al, 4
	call dprintnu8_al_
	mov al, ah
	and al, 0x0f
	call dprintnu8_al_

	pop ax
	ret

dprintBCD_ax_:
	xchg al, ah
	call dprintBCD_al_
	xchg al, ah
	call dprintBCD_al_
	ret

dprinthex_al_:
	push cx
	push es
	push si

	mov cx, ax
	xor ah, ah

	mov si, cs
	mov es, si

	mov si, hex
	mov al, cl
	shr al, 4
	add si, ax
	mov al, [es:si]
	call dprintchar_al_

	mov si, hex
	mov al, cl
	and al, 0x0f
	add si, ax
	mov al, [es:si]
	call dprintchar_al_

	mov ax, cx

	pop si
	pop es
	pop cx
	ret

;**********************************************************************************************
; string and strlist
;**********************************************************************************************
dprintsln_essi_:
	call dprints_essi_
	call dnewline__
	ret

dprints_essi_:
	push ax
	push si

	call dsavetostackdn__

	jmp .skip
	.again:
		inc si
		;call dprintchar_al_
		call dputchar_al_
		call dnextcursorposition__
		.skip:
		mov al, [es:si]
		cmp al, EOS
		jne .again

	call drestorefromstackdn__

	pop si
	pop ax
	ret

dprintl_essial_:
	push di
	push si
	push cx

	mov cx, [es:si]
	add si, 0x02

	mov di, si

	inc cx
	jmp .skip

	.again:
		call dprints_essi_
		call dprintchar_al_
		call slocal_toEOS_esdi_
		inc di
		mov si, di
		.skip:
		loop .again
	._again:

	pop cx
	pop si
	pop di
	ret

dnewline__:
	push ax
	mov al, NEWLINE
	call dprintchar_al_
	pop ax
	ret

dputchar_al_:
	push di

	mov di, [display.cursor_position]
	call dputchar_dial_

	pop di
	ret

dreputchar_di_:
	push ax

	shl di, 1
	mov al, [gs:di]
	shr di, 1
	call dputchar_dial_

	pop ax
	ret

dputchar_dial_:
	push ax

	mov ah, [display.attribute]

	jmp .skip

	cmp al, NEWLINE
	jne .skip

	push bx

	mov bh, ah
	shr bh, 4
	and ah, 0xf0
	or ah, bh

	pop bx

	.skip:
	shl di, 1
	mov [gs:di], ax
	shr di, 1

	pop ax
	ret

dprintchar_al_:
	call dsavetostackdn__
	call dputchar_al_
	call dnextcursorposition__
	call drestorefromstackdn__
	ret

;**********************************************************************************************
; cursor
;**********************************************************************************************
dnextcursorposition__:
	push di

	mov di, [display.cursor_position]
	call dtonextcursorposition_di_

	cmp di, [display.max_cursor_position]
	jng .skip

	push ax
	xor al, al
	call donelinetostackup_al_
	call dupwithoneline__
	call dlastlinefromstackdn__
	
	xor ah, ah
	mov al, [display.columns]
	sub di, ax
	pop ax


	.skip:
	mov [display.cursor_position], di

	call dsetcursorposition__

	pop di

	ret

dbackcursorposition__:
	push di

	mov di, [display.cursor_position]
	call dtobackcursorposition_di_

	test di, di
	jns .skip

	push ax
	mov al, [display.lines]
	dec al
	;call dlastlinetostackdn__
	call donelinetostackdn_al_
	call ddownwithoneline__

	mov al, [display.columns]
	xor ah, ah
	add di, ax
	pop ax

	call dfirstlinefromstackup__

	.skip:
	mov [display.cursor_position], di

	call dsetcursorposition__

	pop di
	ret

dtonextcursorposition_di_:
	push ax

	shl di, 1
	mov al, [gs:di]
	shr di, 1

	cmp al, EOS
	je .end

	cmp al, NEWLINE
	je .newline

	inc di
	jmp .end

	.newline:
		push bx

		mov ax, di
		mov bl, [display.columns]

		div bl

		mul bl
		xor bh, bh
		add ax, bx

		mov di, ax

		pop bx

	.end:
	pop ax
	ret

dtobackcursorposition_di_:
	push ax

	dec di
	sal di, 1

	.again:
		test di, di
		js .sign

		mov al, [gs:di]
		cmp al, EOS
		jne ._again

		sub di, 2

		jmp .again

		.sign:
			call dstackupisempty__ah
			cmp ah, FALSE
			je ._again
			xor di, di

	._again:

	sar di, 1
	pop ax
	ret

dsetcursorposition__:
	push ax
	push dx

	mov al,0x0f
	mov dx,0x03d4		;VGA port 3D4h
	out dx,al             

	mov ax, [display.cursor_position]
	;shr ax, 0x01
 
	inc dx
	out dx,al		;send to VGA hardware

	mov al,0x0e
	dec dx
	out dx,al

	mov al, ah
	inc dx
	out dx,al               ;send to VGA hardware

	pop dx
	pop ax
	ret

dcursorhide__:
	push dx
	push ax

	mov dx, 0x03d4
	mov al, 0x0a
	out dx, al

	inc dx
	in al, dx
	or al, 00100000b
	out dx, al
	
	pop ax
	pop dx
	ret

dcursorshow__:
	push dx
	push ax

	mov dx, 0x03d4
	mov al, 0x0a
	out dx, al

	inc dx
	in al, dx
	and al, 11011111b
	out dx, al
	
	pop ax
	pop dx
	ret

dcursorset_blbh_:
	push ax
	push dx
	push cx
	push bx

	and bl, 00011111b
	and bh, 00011111b

	mov dx, 0x03d4
	mov al, 0x0a
	out dx, al

	inc dx
	in al, dx
	and al, 11100000b
	or al, bl
	out dx, al

	dec dx
	mov al, 0x0b
	out dx, al

	inc dx
	in al, dx
	and al, 11100000b
	or al, bh
	out dx, al

	pop bx

	; columns

	pop cx
	pop dx
	pop ax
	ret

;**********************************************************************************************
; stack
;**********************************************************************************************
donelinetostack_aldx_:
	push cx
	push ax
	push si

	inc al
	xor ch, ch
	mov cl, [display.columns]

	mul cl
	mov si, ax
	dec si
	shl si, 1

	.again:
		mov al, [gs:si]
		call dx

		sub si, 2
		loop .again

	pop si
	pop ax
	pop cx
	ret

donelinetostackup_al_:
	push dx
	mov dx, dstackuppush_al_
	call donelinetostack_aldx_
	pop dx
	ret

donelinetostackdn_al_:
	push dx
	mov dx, dstackdnpush_al_
	call donelinetostack_aldx_
	pop dx
	ret

dfirstlinefromstackup__:
	push cx
	push ax
	push dx
	push di

	mov dx, dstackuppop__al

	mov cl, [display.columns]
	xor ch, ch

	xor di, di

	.again:
		call dx
		cmp al, NEWLINE
		jne .skip

		mov dx, dgetEOS__al
		add sp, 2
		push di

		.skip:
		call dputchar_dial_
		inc di
		loop .again

	pop di
	pop dx
	pop ax
	pop cx
	ret

dlastlinefromstackdn__:
	push di
	push dx
	push cx

	mov dx, dstackdnpop__al

	mov cl, [display.columns]
	xor ch, ch

	mov di, [display.max_cursor_position]
	sub di, cx
	inc di

	.again:
		call dx
		cmp al, NEWLINE
		jne .skip

		mov dx, dgetEOS__al

		.skip:
		call dputchar_dial_
		inc di
		loop .again

	pop cx
	pop dx
	pop ax
	ret

dsavetostackdn__:
	push si
	push di
	push ax

	mov si, [display.max_cursor_position]
	mov di, [display.cursor_position]
	shl si, 1
	shl di, 1

	mov ah, [display.attribute]

	jmp .skip

	.again:
		sub si, 2

		.skip:
		mov al, [gs:si]
		call dstackdnpush_al_

		mov al, EOS
		mov [gs:si], ax

		cmp si, di
		jne .again

	pop ax
	pop di
	pop si
	ret

drestorefromstackdn__:
	push di
	push ax
	push si

	mov di, [display.cursor_position]
	mov si, [display.max_cursor_position]

	.again:
		call dstackdnpop__al

		call dputchar_dial_
		call dtonextcursorposition_di_

		cmp di, si
		jg ._again

		cmp al, EOS
		jne .again
	._again:
	pop si
	pop ax
	pop di
	ret

;**********************************************************************************************
; getters and setters
;**********************************************************************************************
dgetEOS__al:
	mov al, EOS
	ret

dgetchar__al:
	push di
	mov di, [display.cursor_position]
	shl di, 1
	mov al, [gs:di]
	pop di
	ret

dgetattribute__ah:
	push di
	mov di, [display.cursor_position]
	shl di, 1
	inc di
	mov ah, [gs:di]
	pop di
	ret

dsetattribute_ah_:
	push di
	mov di, [display.cursor_position]
	call dsetattribute_diah_
	pop di
	ret

dsetattribute_diah_:
	push di
	shl di, 1
	inc di
	mov [gs:di], ah
	pop di
	ret

;*********************************************************************************************
; key pressed commands
;*********************************************************************************************
dlbackspace__:
	push ax
	mov al, EOS
	call dsavetostackdn__
	call dbackcursorposition__
	call dputchar_al_
	call drestorefromstackdn__
	pop ax
	ret

drbackspace__:
	call dnextcursorposition__
	call ddelete__
	call dbackcursorposition__
	ret

ddelete__:
	push ax
	mov al, EOS
	call dputchar_al_
	call dsavetostackdn__
	call drestorefromstackdn__
	pop ax
	ret

;********************************************************************************************
; scroll functions
;********************************************************************************************

dscrollup__:
	push si
	push es
	push ax

	call dstackupisempty__ah
	cmp ah, TRUE
	je .skip

	mov al, [display.columns]
	xor ah, ah

	mov si, [display.cursor_position]
	add si, ax
	mov [display.cursor_position], si
	call dcursorshoworhide__

	mov al, [display.lines]
	dec al

	call donelinetostackdn_al_
	call ddownwithoneline__
	call dfirstlinefromstackup__

	.skip:
	pop ax
	pop es
	pop si
	ret

dscrolldn__:
	push si
	push es
	push ax

	call dstackdnisempty__ah
	cmp ah, TRUE
	je .skip

	mov al, [display.columns]
	xor ah, ah

	mov si, [display.cursor_position]
	sub si, ax
	mov [display.cursor_position], si
	call dcursorshoworhide__

	mov al, ah
	call donelinetostackup_al_
	call dupwithoneline__
	call dlastlinefromstackdn__

	.skip:
	pop ax
	pop es
	pop si
	ret

dcursorshoworhide__:
	push cx
	push ax

	mov cx, [display.cursor_position]
	mov ax, [display.max_cursor_position]

	call dcursorhide__

	test cx, cx
	js .skip

	cmp cx, ax
	jg .skip

	call dcursorshow__
	call dsetcursorposition__

	.skip:
	pop ax
	pop cx
	ret

;*********************************************************************************************
; temp
;*********************************************************************************************
dgprint_essi_:
	push ax
	push si
	pusha

	mov ah, 0x0e
	.again:
		lodsb
		cmp al, EOS
		je ._again

		xor bh, bh
		mov bl, 00001010b
		mov cx, 0x01

		int 0x10
		jmp .again
	._again:
	pop ax
	pop si
	pop ax
	ret

dputpixel_cxdx_:
	push ax
	push bx

	mov ah, 0x0c
	mov al, 00001111b
	mov bh, 0x0
	int 0x10

	pop bx
	pop ax
	ret