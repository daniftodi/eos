; SEGMENTS:
;	0x0000:0x0000 - 0x0000:0x03ff -> RM interrupt vector
;	0x0000:0x0400 - 0x0000:0x0500 -> bios data area
;	0x0000:0x0500 - 0x0000:0xffff -> stack area

;	0x1000:0x0000 - 0x1000:0xffff -> code area (segment)
;	0x2000:0x0000 - 0x2000:0xffff -> STACK_SEGMENT
;	0x3000:0x0000 - 0x3000:0xffff -> ES
;	0x4000:0x0000 - 0x4000:0xffff -> FS
;	0x5000:0x0000 - 0x5000:0xffff ->
;	0x6000:0x0000 - 0x6000:0xffff ->
;	0x7000:0x0000 - 0x7000:0xffff ->
;	0x8000:0x0000 - 0x8000:0xffff ->

;	0x9000:0x0000 - 0x9000:0xfbff -> 

;	0x9000:0xfc00 - 0x9000:0xffff -> extended bios area
;	0xa000:0x0000 - 0xb000:0xffff -> video memory
;		0xb800:0x0000 - 0xb800:0x0f9f -> GS
;	0xc000:0x0000 - 0xf000:0xffff -> bios area

%include "CONSTANTS.NASM"

jmp CSEGMENT:kernel_start		; call from bootloader

%include "calls_offset.nasm"
%include "calls_name.nasm"

kernel_start:
	cld
	mov [drive], dl

	cli
	mov ax, SSEGMENT
	mov ss, ax
	xor bp, bp
	not bp
	mov sp, bp
	sti

	mov ax, CSEGMENT
	mov ds, ax

	mov ax, FSEGMENT
	mov fs, ax

	mov ax, ESEGMENT
	mov es, ax

	mov ax, GSEGMENT
	mov gs, ax

	call _init__

	call _terminal__

	jmp $

_init__:
	call dsett80x25__
	call dstackset__

	push ax

	mov al, WHITE_ON_BLACK
	mov [display.attribute], al

	xor ax, ax
	mov [display.cursor_position], ax

	call dstackset__
	call dcls__
	
	pop ax
	ret

_terminal__:
	pusha

	mov si, cs
	mov es, si
	mov fs, si

	.again:
		mov si, _whoami_essi_.str
		call dprints_essi_
		mov si, .str
		call dprints_essi_

		mov di, .buffer
		mov dx, cisascii_al_ah
		.nullstring:
		mov cx, READLINE_LEN
		call kreadline_esdidxcx_

		mov si, di

		call slength_essi_cx
		test cx, cx
		jz .nullstring

		call dnewline__

		mov al, ' '
		mov ah, EOS
		call strim_essial_
		call snorepeats_essial_
		call scount_essial_cx

		inc cx
		call sreplace_essialah_

		mov [.strList_length], cx
		
		mov si, calls_name
		call licontains_essifsdi_cxah
		cmp ah, TRUE
		je .skip1

		xor cx, cx

		.skip1:
		mov ax, cx

		;---------------- to history
		call lgeti_essicx_di
		cmp cx, 2
		jl .skip2
		call hpush_di_
		;---------------- to history

		.skip2:
		mov cx, 0x0002
		mul cx

		add ax, calls_offset

		mov di, ax
		mov dx, [di]
		mov si, .strList_length

		call dx

		jmp .again

	popa
	ret
		.str db "> ", EOS
		.strList_length dw 0
		.buffer times (READLINE_LEN + 1) db 0
		
_nocommand_essi_:
	push si
	push es

	mov si, cs
	mov si, .str
	call dprintsln_essi_

	pop es
	pop si
	ret
		.str: db "ERROR! That command not exist!", EOS

_confirmation__ah:
	push es
	push si
	push di
	push cx
	push dx
	mov [.tmp], al

	call dcursorhide__

	mov si, cs
	mov es, si
	mov si, .str

	call dprints_essi_

	mov dx, cisalpha_al_ah
	mov cx, 1
	mov di, .buffer

	.again:
	call kreadline_esdidxcx_

	mov al, [cs:di]

	cmp al, EOS
	je .again

	mov ah, TRUE
	cmp al, 'y'
	je .skip
	cmp al, 'Y'
	je .skip

	mov ah, FALSE
	cmp al, 'n'
	je .skip
	cmp al, 'N'
	je .skip

	cmp al, ASCII_KEY_ESC
	je .skip

	call dlbackspace__
	jmp .again

	.skip:

	call dcursorshow__

	mov al, [.tmp]
	pop dx
	pop cx
	pop di
	pop si
	pop es
	ret
		.str db "you are sure ? (y or n)", EOS
		.tmp db 0
		.buffer db 0, 0

_repeat_dxcx_:
	push cx

	inc cx
	jmp .skip

	.again:
		call dx
		.skip:
		loop .again

	pop cx
	ret

_nullfunction__:
	ret

;*************************************************************************************************
; include
;*************************************************************************************************

%include "source/kernel/drivers/display/stack.nasm"
%include "source/kernel/drivers/display/color.nasm"
%include "source/kernel/drivers/display/display.nasm"
%include "source/kernel/drivers/keyboard/keyboard.nasm"
%include "source/kernel/include/char/char.nasm"
%include "source/kernel/include/string/string.nasm"
%include "source/kernel/include/strList/strlist.nasm"
%include "source/kernel/include/number/number.nasm"
%include "source/kernel/programs/all_programs.nasm"
%include "source/kernel/interrupts/all_interrupts.nasm"

;***********************************************************************
; variables

drive: db 0

yes: db "YES", EOS
no: db "NO", EOS

;***********************************************************************

times 10240 - ($ - $$) db 0
