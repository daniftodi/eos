; Windows in memory (window.segment):
;	n[window1][window2][...][windown]

; Window format:
;	pctbn ----> 									total 30 bytes
;   |||||
;   |||| -----> string name 						WINDOW_NAME_LENGTH( = 20 bytes) + 1 bytes
;   ||| ------> bottom right x, y					2 bytes
;   || -------> top left x, y						2 bytes
;   | --------> content pointer, segment:offset		4 bytes
;    ---------> pos = [1,n]							1 byte

window:
	.segment		dw 	0x0
	.max			db 	0x0


winit__:
	push si
	push bx
	push dx
	push ax

	mov di, WINDOW_LIST_SEGMENT
	mov [window.segment], di
	mov fs, di
	xor di, di
	mov byte [fs:di], 0x0

	mov dx, cx

	mov cl, WINDOW_LIST_MAX
	mov [window.max], cl

	xor ch, ch
	inc di

	mov ah, 10000111b
	xor al, al

	mov bx, WINDOW_CONTENT_OFFSET

	.again:
		mov [fs:di], al
		mov [fs:di + WINDOW_FORMAT_CONTENT], fs
		mov [fs:di + WINDOW_FORMAT_CONTENT + 2], bx

		mov [fs:bx], ah ; attribute

		xchg di, bx
		call wclearcontent_fsdi_
		xchg di, bx

		add di, WINDOW_FORMAT_SIZE
		add bx, WINDOW_CONTENT_SIZE

		loop .again

	mov cx, dx

	;mov si, main_window
	xor bx, bx
	mov dx, 0x184f
	call wnew_blbhdldhessi_fsdiah
	call wdraw_fsdi_
	call wsetcurrent_essi_ah

	pop ax
	pop dx
	pop bx
	pop si
	ret
	

wfindbyname_essi_fsdiah:
	push fs
	push di
	push cx
	
	mov di, [window.segment]
	mov fs, di
	xor di, di
	mov cl, [fs:di]
	inc di

	mov ah, FALSE

	.again:
		test cl, cl
		jz ._again

		mov ch, [fs:di]
		test ch, ch
		jz .skip

		add di, WINDOW_FORMAT_SIZE - WINDOW_NAME_LENGTH - 1
		call sisequal_essifsdi_ah
		sub di, WINDOW_FORMAT_SIZE - WINDOW_NAME_LENGTH - 1
		cmp ah, TRUE
		je ._again
		dec cl

		.skip:
		add di, WINDOW_FORMAT_SIZE
		jmp .again
	._again:
	pop cx

	cmp ah, TRUE
	je .new

	pop di
	pop fs
	ret

	.new:
	add sp, 4
	ret

wfindbypos_al_fsdiah:
	push cx

	mov cx, [window.segment]
	mov fs, cx

	xor di, di
	mov cl, [fs:di]
	mov ah, FALSE

	inc di

	.again:
		test cl, cl
		jz .end

		cmp al, [fs:di]
		je ._again

		dec cl
		add di, WINDOW_FORMAT_SIZE
		jmp .again
	._again:
	mov ah, TRUE
	.end:
	pop cx
	ret

wnotexistname_essi_ah:
	push dx
	mov dx, wfindbyname_essi_fsdiah
	call wnotexist_dx_ah
	pop dx
	ret

wnotexistpos_al_ah:
	push dx
	mov dx, wfindbypos_al_fsdiah
	call wnotexist_dx_ah
	pop dx
	ret

wnotexist_dx_ah:
	push fs
	push di

	call dx

	cmp ah, TRUE
	je .true

	mov ah, TRUE
	jmp .end

	.true:
	mov ah, FALSE

	.end:
	pop di
	pop fs
	ret

wnew_essi_fsdiah:
	mov [.tmp], al

	mov ax, [window.segment]
	mov fs, ax

	mov ah, FALSE

	; verify if current number not equal with max number
	mov al, [fs:0x0]
	cmp al, [window.max]
	je .end

	; verify if window with same name not existent
	call wnotexistname_essi_ah
	cmp ah, FALSE
	je .end

	inc al
	mov [fs:0x0], al

	;set window number(pos)
	mov di, 0x01						; from first wind

	.again:
		;test al, al
		;jz ._again

		mov ah, [fs:di]
		test ah, ah
		jz ._again

		;dec al
		add di, WINDOW_FORMAT_SIZE
		jmp .again
	._again:
	mov al, [fs:0x0]
	mov [fs:di], al

	mov ah, TRUE

	.end:
	mov al, [.tmp]
	ret
		.tmp db 0x0

wnew_blbhdldhessi_fsdiah:
	;bl(up left x), bh(up left y), dl(bottom right x), dh(bottom right y), essi(window name),
	call wnew_essi_fsdiah
	cmp ah, FALSE
	je .end

	push di

	inc di
	;mov [fs:di], content_segment
	add di, 0x02
	;mov [fs:di], content_offset

	add di, 0x02
	mov [fs:di], bx

	add di, 0x02
	mov [fs:di], dx

	add di, 0x02
	call scopy_essifsdi_

	pop di

	;call wdraw_fsdi_

	.end:
	ret

wdelbyname_essi_:
	push dx
	mov dx, wfindbyname_essi_fsdiah
	call wdel_dx_
	pop dx
	ret

wdelbypos_al_:
	push dx
	mov dx, wfindbypos_al_fsdiah
	call wdel_dx_
	pop dx
	ret

wdel_dx_:
	push fs
	push di
	push ax

	call dx

	cmp ah, FALSE
	je .end

	mov al, [fs:di]
	call wdecposgt_al_

	mov al, 0x0
	mov [fs:di], al

	mov al, [fs:0x0]
	dec al
	mov [fs:0x0], al

	.end:
	pop ax
	pop di
	pop fs
	ret

wdecposgt_al_:
	push di
	push cx

	xor di, di
	mov cl, [fs:di]
	inc di

	.again:
		test cl, cl
		jz ._again

		mov ch, [fs:di]
		test ch, ch
		jz .skip

		cmp al, ch
		jns .no

		dec ch
		mov [fs:di], ch

		.no:
		dec cl

		.skip:
		add di, WINDOW_FORMAT_SIZE
		jmp .again
	._again:

	pop cx
	pop di
	ret

wsetcurrent_essi_ah:
	call wfindbyname_essi_fsdiah
	cmp ah, FALSE
	je .end

	push bx
	mov bl, al
	mov al, [fs:di]
	call wdecposgt_al_
	mov al, bl

	mov bl, [fs:0x0]
	mov [fs:di], bl

	mov bl, [fs:di + WINDOW_FORMAT_TL]
	;mov [display.ccursor], bl
	mov bl, [fs:di + WINDOW_FORMAT_TL + 1]
	;mov [display.lcursor], bl

	pop bx

	.end:
	ret

wdraw_fsdi_:
	call wdrawtopline_fsdi_
	call wdrawbottomline_fsdi_
	call wdrawleftline_fsdi_
	call wdrawrightline_fsdi_
	ret

wdrawtopline_fsdi_:
	push bx

	mov bx, [fs:di + WINDOW_FORMAT_TL]
	dec bh
	dec bl
	
	test bh, bh
	js .end

	push si

	mov al, DCORNERTL
	test bl, bl
	js .preagain

	call wlocal_calc_blbh_si
	mov [gs:si], al

	add si, 0x02
	inc bl

	jmp .__preagain

	.preagain:
		test bl, bl
		jns ._preagain

		inc bl
		jmp .preagain
	._preagain:

	call wlocal_calc_blbh_si

	.__preagain:

	mov bh, [fs:di + WINDOW_FORMAT_BR]
	inc bh

	mov al, DHLINE
	
	cmp bh, 79
	jng .again

	mov bh, 80

	.again:
		cmp bl, bh
		je ._again

		mov [gs:si], al
		add si, 0x02
		inc bl
		jmp .again

	._again:

	cmp bl, 80
	jge .nocorneltr

	mov al, DCORNERTR
	mov [gs:si], al

	.nocorneltr:

	pop si
	.end:
	pop bx
	ret

wdrawbottomline_fsdi_:
	push bx

	mov bx, [fs:di + WINDOW_FORMAT_BR]
	inc bh
	inc bl

	cmp bh, 24
	jg .end

	push si

	cmp bl, 80
	jns .preagain

	call wlocal_calc_blbh_si
	mov al, DCORNERBR
	mov [gs:si], al
	dec bl
	sub si, 0x02

	jmp .__preagain

	.preagain:
		cmp bl, 80
		js ._preagain

		dec bl
		jmp .preagain
	._preagain:

	call wlocal_calc_blbh_si

	.__preagain:

	mov bh, [fs:di + WINDOW_FORMAT_TL]
	dec bh

	mov al, DHLINE
	
	test bh, bh
	jns .again

	xor bh, bh
	dec bh

	.again:
		cmp bl, bh
		je ._again

		mov [gs:si], al
		sub si, 0x02
		dec bl
		jmp .again

	._again:

	test bl, bl
	js .nocornerbl

	mov al, DCORNERBL
	mov [gs:si], al

	.nocornerbl:

	pop si
	.end:
	pop bx
	ret

wdrawleftline_fsdi_:
	push bx

	mov bx, [fs:di + WINDOW_FORMAT_TL]
	;dec bh
	dec bl
	
	test bl, bl
	js .end

	.preagain:
		test bh, bh
		jns ._preagain

		inc bh
		jmp .preagain
	._preagain:

	push si

	call wlocal_calc_blbh_si

	mov bl, [fs:di + WINDOW_FORMAT_BR + 1]

	mov al, DVLINE
	
	cmp bl, 24
	jng .again

	mov bl, 24

	.again:
		cmp bh, bl
		jg ._again

		mov [gs:si], al
		add si, 160
		inc bh
		jmp .again

	._again:
	pop si
	.end:
	pop bx
	ret

wdrawrightline_fsdi_:
	push bx

	mov bx, [fs:di + WINDOW_FORMAT_BR]
	;inc bh
	inc bl

	cmp bl, 79
	jg .end

	.preagain:
		cmp bh, 25
		jl ._preagain

		dec bh
		jmp .preagain
	._preagain:

	push si

	call wlocal_calc_blbh_si

	mov bl, [fs:di + WINDOW_FORMAT_TL + 1]

	mov al, DVLINE
	
	test bl, bl
	jns .again

	xor bl, bl

	.again:
		cmp bh, bl
		jl ._again

		mov [gs:si], al
		sub si, 160
		dec bh
		jmp .again

	._again:
	pop si
	.end:
	pop bx
	ret

wcls_fsdi_:
	push di

	mov di, [fs:di + WINDOW_FORMAT_CONTENT + 2]		; offset content

	call wclearcontent_fsdi_

	pop di

	;call dcls__

	ret

wclearcontent_fsdi_:
	push di
	push cx
	push ax

	mov al, EOS
	mov cx, WINDOW_CONTENT_SIZE - 1

	inc di

	.again:
		mov [fs:di], al
		inc di
		loop .again

	pop ax
	pop cx
	pop di
	ret


wprintcontent_fsdi_:
	push es
	push si
	push ax
	push cx

	mov si, [fs:di + WINDOW_FORMAT_CONTENT]
	mov es, si

	mov si, [fs:di + WINDOW_FORMAT_CONTENT + 2]
	;mov ah, [es:si]
	mov ax, si
	call dprintnu16_ax_

	inc si
	mov cx, WINDOW_CONTENT_SIZE
	dec cx

	.again:
		mov al, [es:si]
		cmp al, EOS
		je ._again

		;call dputchar_al_
		loop .again

	._again:

	pop cx
	pop ax
	pop si
	pop es
	ret

wlocal_calc_blbh_si:
	; si = (bh * 80 + bl) * 2 = 
	;	 = (bh * 2 * 2 * 2 * 2 * 5 + bl) * 2 = 
	;	 = ((bh << 4) * 5 + bl) * 2
	push bx

	shr bx, 8
	mov si, bx
	shl si, 4

	mov bx, si
	add si, si
	add si, si
	add si, bx

	pop bx
	push bx

	xor bh, bh

	add si, bx
	shl si, 1
	pop bx
	ret


;**************************************************************************************************
; window errors string
;**************************************************************************************************

