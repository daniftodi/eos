;*******************************************************************************
; 8 bits
;*******************************************************************************
nu8tostr_alesdi_:
	push ax
	push di
	mov byte [.tmp], bl

	mov bl, 0x0a
	xor ah, ah

	cmp al, 0x0
	je .skip

	.again:
		cmp al, 0x0
		je ._again

		div bl

		.skip:
		or ah, 00110000b
		mov [es:di], ah

		xor ah, ah
		inc di
		jmp .again
	._again:
	mov al, EOS
	mov [es:di], al

	mov bl, [.tmp]
	pop di
	push si

	mov si, di
	call sreverse_essi_

	pop si
	pop ax
	ret
		.tmp db 0

ns8tostr_alesdi_:
	push ax
	mov ah, al

	call ns8abs_al_

	call nu8tostr_alesdi_
	cmp ah, 0x80
	js .skip

	push cx
	push si

	xor cx, cx
	mov si, di
	mov al, '-'
	call sinsertchar_essialcx_

	pop si
	pop cx

	.skip:
	pop ax
	ret

ns8abs_al_:
	test al, al
	jns .skip
	call ns8opposed_al_
	.skip:
	ret

ns8opposed_al_:
	xor al, 0xff
	inc al
	ret

;*******************************************************************************
; 16 bits
;*******************************************************************************

nu16tostr_axesdi_:
	push ax
	push dx
	push di
	mov word [.tmp], bx

	mov bx, 0x0a
	xor dx, dx

	cmp ax, 0x0
	je .skip

	.again:
		cmp ax, 0x0
		je ._again

		div bx

		.skip:
		or dl, 00110000b
		mov [es:di], dl

		xor dl, dl
		inc di
		jmp .again
	._again:
	mov al, EOS
	mov [es:di], al

	mov word bx, [.tmp]
	pop di
	push si

	mov si, di
	call sreverse_essi_

	pop si
	pop dx
	pop ax
	ret
		.tmp dw 0

ns16tostr_axesdi_:
	push ax

	call ns16abs_ax_

	call nu16tostr_axesdi_

	pop ax
	cmp ah, 0x80
	js .skip

	push ax
	push cx
	push si

	xor cx, cx
	mov si, di
	mov al, '-'
	call sinsertchar_essialcx_

	pop si
	pop cx
	pop ax

	.skip:
	ret

ns16abs_ax_:
	test ah, ah
	jns .skip
	call ns16opposed_ax_
	.skip:
	ret

ns16opposed_ax_:
	xor ax, 0xffff
	inc ax
	ret













