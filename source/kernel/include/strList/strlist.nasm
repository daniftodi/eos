lcopy_essifsdi_:
	push si
	push di
	push cx
	push dx

	mov dx, [es:si]
	mov [fs:di], dx
	add si, 0x02
	add di, 0x02

	.again:
		test dx, dx
		jz ._again

		call scopy_essifsdi_cx

		add si, cx
		inc si
		add di, cx
		inc di

		dec dx
		jmp .again
	._again:

	pop dx
	pop cx
	pop di
	pop si
	ret


ltostr_essifsdi_:
	push si
	push di
	push dx
	push cx

	mov dx, [es:si]
	add si, 0x02

	.again:
		test dx, dx
		jz ._again

		call scopy_essifsdi_cx

		add si, cx
		add di, cx
		inc si
		dec dx
		jmp .again
	._again:

	pop cx
	pop dx
	pop di
	pop si
	ret

licontains_essifsdi_cxah:
	push dx
	mov dx, siisequal_essifsdi_ah
	call lcontains_essifsdidx_cxah
	pop dx
	ret

lcontains_essifsdi_cxah:
	push dx
	mov dx, sisequal_essifsdi_ah
	call lcontains_essifsdidx_cxah
	pop dx
	ret

lcontains_essifsdi_ah:
	push cx
	call lcontains_essifsdi_cxah
	pop cx
	ret

lcontains_essifsdidx_cxah:
	push si
	push bx
	mov [.tmp], al

	mov bx, [es:si]
	xor cx, cx
	add si, 0x02

	mov ah, FALSE

	jmp .skip
	.again:
		xchg si, di
		call slocal_toEOS_esdi_
		inc di
		xchg si, di

		inc cx

		.skip:
		cmp bx, cx
		je ._again

		call dx
		cmp ah, FALSE
		je .again

	._again:
	mov al, [.tmp]
	pop bx
	pop si
	ret
		.tmp db 0

lgeti_essicx_di:
	mov di, si
	add di, 2

	cmp cx, [es:si]
	jg .end

	push cx
	dec di
	inc cx

	jmp .skip
	.again:
		call slocal_toEOS_esdi_
		.skip:
		inc di
		loop .again

	pop cx
	.end:
	ret


