ctolower_al_:
	mov [.tmp], ah
	call cisupper_al_ah

	cmp ah, FALSE
	je .end

	or al, 00100000b

	.end:
	mov ah, [.tmp]
	ret
		.tmp db 0

ctoupper_al_:
	mov [.tmp], ah
	call cislower_al_ah

	cmp ah, FALSE
	je .end

	and al, 11011111b

	.end:
	mov ah, [.tmp]
	ret
		.tmp db 0

cisupper_al_ah:
	push dx

	mov dh, 'A'
	mov dl, 'Z'
	call clocal_isbetween_aldhdl_ah

	pop dx
	ret

cislower_al_ah:
	push dx

	mov dh, 'a'
	mov dl, 'z'
	call clocal_isbetween_aldhdl_ah

	pop dx
	ret

cisalpha_al_ah:
	mov [.tmp], al
	or al, 00100000b

	call cislower_al_ah
	mov al, [.tmp]
	ret
		.tmp db 0

cisprint_al_ah:
	push dx

	mov dh, '!'
	mov dl, '~'
	call clocal_isbetween_aldhdl_ah

	pop dx
	ret

cisdigit_al_ah:
	push dx

	mov dh, '0'
	mov dl, '9'
	call clocal_isbetween_aldhdl_ah

	pop dx
	ret

cishex_al_ah:
	call cisdigit_al_ah
	cmp ah, TRUE
	je .end

	push dx

	mov [.tmp], al
	or al, 00100000b
	mov dh, 'a'
	mov dl, 'f'
	call clocal_isbetween_aldhdl_ah

	mov al, [.tmp]

	pop dx

	.end:
	ret
		.tmp db 0

cisascii_al_ah:
	push dx

	mov dh, 0x0
	mov dl, 01111111b
	call clocal_isbetween_aldhdl_ah

	pop dx
	ret


;************************************************************
; local functions

clocal_isbetween_aldhdl_ah:
	mov ah, FALSE

	cmp al, dh
	jl .end

	cmp al, dl
	ja .end

	mov ah, TRUE

	.end:
	ret
