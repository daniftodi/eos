scopy_essifsdi_cx:
	mov [.tmp], al
	xor cx, cx
	cld
	.again:
		inc cx
		mov al, [es:si]
		mov [fs:di], al

		inc si
		inc di

		cmp al, EOS
		jne .again
	mov al, [.tmp]
	sub si, cx
	sub di, cx
	dec cx
	ret
		.tmp db 0

scopy_essifsdi_:
	push cx
	call scopy_essifsdi_cx
	pop cx
	ret

smove_essifsdi_:
	call scopy_essifsdi_
	call sdelete_essi_
	ret

sdelete_essi_:
	mov byte [es:si], EOS
	ret

stoupper_essi_:
	push si
	mov [.tmp], al
	.again:
		mov al, [es:si]
		cmp al, EOS
		je ._again

		call ctoupper_al_
		mov [es:si], al

		inc si
		jmp .again
	._again:
	mov al, [.tmp]
	pop si
	ret
		.tmp db 0

stolower_essi_:
	push si
	mov [.tmp], al
	.again:
		mov al, [es:si]
		cmp al, EOS
		je ._again

		call ctolower_al_
		mov [es:si], al

		inc si
		jmp .again
	._again:
	mov al, [.tmp]
	pop si
	ret
		.tmp db 0

sreverse_essi_:
	push si
	push di
	push ax

	mov di, si
	call slocal_toEOS_esdi_

	dec di
	.again:
		cmp si, di
		jae ._again

		mov al, [es:si]
		mov ah, [es:di]
		mov [es:si], ah
		mov [es:di], al

		inc si
		dec di
		jmp .again
	._again:
	pop ax
	pop di
	pop si
	ret

slength_essi_cx:
	mov cx, di
	mov di, si

	call slocal_toEOS_esdi_

	sub di, si

	xchg cx, di
	ret

sconcat_essifsdi_:
	push si

	xchg si, di
	;esdi, fssi

	call slocal_toEOS_esdi_

	push es
	push fs
	pop es
	pop fs
	;fsdi, essi

	call scopy_essifsdi_

	mov di, si
	push es
	push fs
	pop es
	pop fs

	pop si
	;essi, esdi
	ret

strim_essial_:
	call striml_essial_
	call strimr_essial_
	ret

striml_essial_:
	push si
	push di

	mov byte [.tmp], ah

	mov di, si
	dec si
	.again:
		inc si
		mov ah, [es:si]
		cmp ah, EOS
		je ._again
		
		cmp ah, al
		je .again
	._again:
	cmp di, si
	je .skip

	push fs
	push es
	pop fs

	call scopy_essifsdi_

	pop fs

	.skip:
	mov byte ah, [.tmp]
	pop di
	pop si
	ret
		.tmp db 0

strimr_essial_:
	push di
	mov byte [.tmp], ah

	mov di, si
	call slocal_toEOS_esdi_

	.again:
		dec di
		cmp si, di
		ja ._again

		mov ah, [es:di]
		cmp ah, al
		je .again
	._again:
	inc di
	mov ah, EOS
	mov [es:di], ah
	mov ah, [.tmp]
	pop di
	ret
		.tmp db 0

sreplace_essialah_:
	push cx
	mov cx, 0xfffe
	call sreplacen_essialahcx_
	pop cx
	ret

sreplacen_essialahcx_:
	cmp al, ah
	je .end

	;cmp ah, EOS
	;jne .skip

	;call sdeln_essialcx_

	;jmp .end
	;.skip:
	push si
	push cx

	mov byte [.tmp], dl
	mov dl, ah
	.again:
		test cx, cx
		jz ._again

		call slocal_toal_essial_ah

		cmp ah, FALSE
		je ._again

		mov [es:si], dl
		dec cx
		inc si

		jmp .again
	._again:
	pop cx
	pop si

	mov ah, dl
	mov byte dl, [.tmp]
	.end:
	ret
		.tmp db 0

sdeln_essialcx_:
	push si
	push di
	push ax
	push cx
	mov di, si
	dec di
	.again:
		inc di
		mov ah, [es:di]
		cmp ah, EOS
		je ._again

		cmp ah, al
		jne .skip

		test cx, cx
		jz .skip

		dec cx
		jmp .again

		.skip:
		mov [es:si], ah
		inc si

		jmp .again
	._again:
	mov [es:si], ah
	pop cx
	pop ax
	pop di
	pop si
	ret

scount_essial_cx:
	push si
	mov byte [.tmp], ah
	xor cx, cx
	jmp .skip
	.again:
		inc si
		inc cx
		.skip:
		call slocal_toal_essial_ah
		cmp ah, TRUE
		je .again
	mov byte ah, [.tmp]
	pop si
	ret
		.tmp db 0

sfilter_essidx_:
	push si
	push di
	push ax
	mov di, si
	dec di
	.again:
		inc di
		mov al, [es:di]
		cmp al, EOS
		je ._again

		call dx				; filter
		cmp ah, FALSE
		je .again

		mov [es:si], al
		inc si

		jmp .again
	._again:
	mov [es:si], al
	pop ax
	pop di
	pop si
	ret

sisfiltered_essidx_ah:
	push si
	mov [.tmp], al
	mov ah, TRUE
	dec si
	.again:
		inc si
		mov al, [es:si]
		cmp al, EOS
		je ._again

		call dx				; filter
		cmp ah, TRUE
		je .again
	._again:
	mov al, [.tmp]
	pop si
	ret
		.tmp db 0

snorepeats_essial_:
	push si
	push di
	push cx
	mov byte [.tmp], ah
	mov di, si
	dec di
	mov ah, EOS
	.again:
		inc di
		mov cl, ah
		mov ah, [es:di]
		cmp ah, EOS
		je ._again

		cmp ah, al
		jne .skip

		cmp ah, cl
		je .again

		.skip:
		mov [es:si], ah
		inc si

		jmp .again
	._again:
	mov [es:si], ah
	mov byte ah, [.tmp]
	pop cx
	pop di
	pop si
	ret
		.tmp db 0

snorepeats_essi_:
	push si
	push di
	push ax
	mov di, si
	dec di
	mov ah, EOS
	.again:
		inc di
		mov al, ah
		mov ah, [es:di]
		cmp ah, EOS
		je ._again

		cmp ah, al
		je .again

		mov [es:si], ah
		inc si

		jmp .again
	._again:
	mov [es:si], ah
	pop ax
	pop di
	pop si
	ret
	
ssplit_essifsdial_:
	push si
	push cx
	push ax
	push di
	add di, 0x02
	xor cx, cx
	.again:
		mov ah, [es:si]
		cmp ah, EOS
		je ._again

		cmp ah, al
		jne .skip

		mov ah, EOS
		inc cx

		.skip:
		mov [fs:di], ah
		inc di
		inc si
		jmp .again
	._again:
	test cx, cx
	jz .no
	mov [fs:di], ah
	inc cx
	.no:
	pop di
	mov word [fs:di], cx
	pop ax
	pop cx
	pop si
	ret

scompare_essifsdi_ah:
	push si
	push di
	mov [.tmp], al
	.again:
		mov ah, [es:si]
		mov al, [fs:di]
		sub ah, al
		jnz ._again

		cmp al, EOS
		je ._again

		inc si
		inc di
		jmp .again
	._again:
	mov al, [.tmp]
	pop di
	pop si
	ret
		.tmp db 0

sicompare_essifsdi_ah:
	push si
	push di
	mov [.tmp], al
	.again:
		mov al, [es:si]
		call ctoupper_al_
		mov ah, al
		mov al, [fs:di]
		call ctoupper_al_

		sub ah, al
		jnz ._again

		cmp al, EOS
		je ._again

		inc si
		inc di
		jmp .again
	._again:
	mov al, [.tmp]
	pop di
	pop si
	ret
		.tmp db 0

sisgreater_essifsdi_ah:
	call scompare_essifsdi_ah
	cmp ah, 0x0
	mov ah, TRUE
	jg .skip
	mov ah, FALSE
	.skip:
	ret

sisless_essifsdi_ah:
	call scompare_essifsdi_ah
	test ah, ah
	mov ah, TRUE
	js .skip
	mov ah, FALSE
	.skip:
	ret

sisequal_essifsdi_ah:
	call scompare_essifsdi_ah
	test ah, ah
	mov ah, TRUE
	jz .skip
	mov ah, FALSE
	.skip:
	ret

siisequal_essifsdi_ah:
	call sicompare_essifsdi_ah
	test ah, ah
	mov ah, TRUE
	jz .skip
	mov ah, FALSE
	.skip:
	ret

slencompare_essifsdi_ah:
	push cx
	push dx
	push es

	call slength_essi_cx
	mov dx, cx
	; dx(essi length)

	mov cx, fs
	mov es, cx
	xchg si, di
	; essi(fsdi)
	call slength_essi_cx
	; cx(fsdi length)

	xchg si, di
	pop es

	sub dx, cx

	mov ah, 0x0
	test dx, dx
	jz .skip

	mov ah, 0xf0
	jl .skip

	mov ah, 0x0f

	.skip:
	pop dx
	pop cx
	ret

slenisequal_essifsdi_ah:
	call slencompare_essifsdi_ah
	test ah, ah
	mov ah, TRUE
	jz .skip
	mov ah, FALSE
	.skip:
	ret

slenisgreater_essifsdi_ah:
	call slencompare_essifsdi_ah
	cmp ah, 0x0
	mov ah, TRUE
	jg .skip
	mov ah, FALSE
	.skip:
	ret

slenisless_essifsdi_ah:
	call slencompare_essifsdi_ah
	test ah, ah
	mov ah, TRUE
	js .skip
	mov ah, FALSE
	.skip:
	ret

sstartwith_essifsdi_ah:
	push si
	push di
	mov ah, FALSE
	jmp .skip
	.again:
		inc di
		inc si
		.skip:
		mov al, [fs:di]
		cmp al, EOS
		je ._again

		cmp al, [es:si]
		je .again

		jmp .not
	._again:
	mov ah, TRUE
	.not:
	pop di
	pop si
	ret

sendswith_essifsdi_ah:
	call slenisless_essifsdi_ah
	cmp ah, FALSE
	je .skip

	mov ah, FALSE
	ret

	.skip:
	push dx
	push cx
	push es

	call slength_essi_cx
	mov dx, cx
	; dx(essi length)

	mov cx, fs
	mov es, cx

	xchg si, di

	call slength_essi_cx

	pop es
	xchg si, di

	sub dx, cx

	add si, dx
	pop cx

	call sisequal_essifsdi_ah
	sub si, dx
	pop dx

	.end:
	ret

sdelsubstr_essicxdx_:
	push di
	push fs

	mov di, es
	mov fs, di
	mov di, si

	add di, cx
	add si, dx

	call scopy_essifsdi_

	sub si, dx

	pop fs
	pop di
	ret

sreplacesubstr_essicxdxesdi_:
	ret

sinsertchar_essialcx_:
	push si
	push cx

	dec si
	inc cx

	.loop:
		inc si
		loop .loop

	pop cx
	push ax

	.again:
		mov ah, [es:si]
		mov [es:si], al
		
		cmp al, EOS
		je ._again

		mov al, ah
		inc si
		jmp .again
	._again:
	pop ax
	pop si
	ret

stonu8_essi_al:
	push si
	push bx
	mov [.tmp], ah

	mov bl, 0x0a
	xor al, al
	.again:
		mov bh, [es:si]
		cmp bh, EOS
		je ._again

		mul bl

		and bh, 0x0f
		add al, bh

		inc si
		jmp .again
	._again:

	mov ah, [.tmp]
	pop bx
	pop si
	ret
		.tmp db 0

stons8_essi_al:
	xor al, al
	mov byte [.tmp], al

	mov al, [es:si]

	cmp al, '-'
	jne .plus
		inc si
		mov byte [.tmp], al
		jmp .skip
	.plus:
	cmp al, '+'
	jne .skip
		inc si
		mov byte [.tmp], al
	.skip:
	call stonu8_essi_al

	mov byte [.tmp + 1], ah
	mov byte ah, [.tmp]

	cmp ah, '-'
	jne .plus2

	dec si
	call ns8opposed_al_
	jmp .skip2

	.plus2:
	cmp ah, '+'
	jne .skip2
		dec si
	.skip2:
	mov ah, [.tmp]
	ret
		.tmp dw 0

stonu16_essi_ax:
	push si
	push bx
	push dx
	push cx

	mov bx, 0x0a
	xor ax, ax
	.again:
		mov cl, [es:si]
		cmp cl, EOS
		je ._again

		mul bx

		and cl, 0x0f
		add al, cl
		adc ah, 0x0

		inc si
		jmp .again
	._again:
	pop cx
	pop dx
	pop bx
	pop si
	ret

stons16_essi_ax:
	xor al, al
	mov byte [.tmp], al

	mov al, [es:si]

	cmp al, '-'
	jne .plus
		inc si
		mov byte [.tmp], al
		jmp .skip
	.plus:
	cmp al, '+'
	jne .skip
		inc si
		mov byte [.tmp], al
	.skip:
	call stonu16_essi_ax

	mov byte [.tmp + 1], bl
	mov byte bl, [.tmp]

	cmp bl, '-'
	jne .plus2

	dec si
	call ns16opposed_ax_
	jmp .skip2

	.plus2:
	cmp bl, '+'
	jne .skip2
		dec si
	.skip2:
	mov bl, [.tmp]
	ret
		.tmp dw 0

;sissubstr_essiesdi_ahcx:
;	push si
;	push di
;	push ax
;	push bx
;	push dx
;	xor cx, cx
;	mov al, TRUE
;	.again:
;		mov al, [es:si]
;		cmp al, EOS
;		je .not
;
;		mov ah, [es:di]
;		cmp ah, al
;		jne .skip
;
;		
;
;		.skip:
;		inc si
;		inc cx
;		jmp .again
;
;		.not:
;		mov al, FALSE
;	._again:
;	mov ah, dl
	

;*****************************************************************
; local functions

slocal_toEOS_esdi_:
	push ax
	.again:
		mov al, [es:di]
		cmp al, EOS
		je .end

		inc di
		jmp .again
	.end:
	pop ax
	ret

slocal_toal_essial_ah:
	.again:
		mov ah, [es:si]
		cmp ah, EOS
		je .not

		cmp ah, al
		je ._again

		inc si
		jmp .again

		.not:
		mov ah, FALSE
		jmp .end
	._again:
	mov ah, TRUE
	.end:
	ret
