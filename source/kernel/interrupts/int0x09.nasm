old0x09:
	.offset 	dw 	0
	.segment 	dw 	0

new0x09:
	.offset 	dw 	int0x09handler
	.segment 	dw 	CSEGMENT

int0x09set__:
	push es
	push si
	push di
	push ax

	mov al, 0x09
	mov si, cs
	mov es, si

	mov di, old0x09
	call intget_al_esdi
	
	mov si, new0x09
	call intset_alessi_

	pop ax
	pop di
	pop si
	pop es
	ret

int0x09reset__:
	push es
	push si
	push di
	push ax

	mov al, 0x09
	mov si, cs
	mov es, si

	mov di, new0x09
	call intget_al_esdi
	
	mov si, old0x09
	call intset_alessi_

	pop ax
	pop di
	pop si
	pop es
	ret

int0x09handler:
	cli
	push ax
	push es
	push si
	pushf

	in al, 0x61
	mov ah, al

	mov [gs:0x0], al

	or al, 0x80
	out 0x61, al
	xchg ah, al
	out 0x61, al

	mov al, 0x20
	out 0x20, al

	mov si, cs
	mov es, si
	mov si, .str

	call dprintsln_essi_

	popf
	pop si
	pop es
	pop ax
	sti
	iret
		.str db "S-a tastat!", EOS



 ;push    ax
  ;       in      al,60H             ;read the key
   ;      cmp     al,POP_KEY         ;is this the hot key?
    ;     je      do_pop             ; yes, trigger the popup
     ;                               ;  no, drop through to original driver
;         pop     ax
;         jmp     cs:[int9_vect]     ;just hop out to original int handler;
;
; do_pop: ;------ following housekeeping is needed to satisfy the hdwr int

 ;        in      al,61H             ;get value of keyboard control lines
 ;        mov     ah,al              ; save it
 ;        or      al,80h             ;set the "enable kbd" bit
 ;        out     61H,al             ; and write it out the control port
 ;        xchg    ah,al              ;fetch the original control port value
 ;        out     61H,al             ; and write it back
;
;         mov     al,20H             ;send End-Of-Interrupt signal
;         out     20H,al             ; to the 8259 Interrupt Controller
         ;------ other code handles other tests and finally triggers popup