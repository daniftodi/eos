intget_al_esdi:
	push fs
	push ax
	push si

	mov ah, 4
	mul ah

	xor si, si
	mov fs, si
	mov si, ax

	mov ax, [fs:si]
	mov [es:di], ax
	call dnewline__
	call dprintnu16_ax_
	mov ax, [fs:si + 2]
	mov [es:di + 2], ax
	call dnewline__
	call dprintnu16_ax_

	call dnewline__

	pop si
	pop ax
	pop fs
	ret

intset_alessi_:
	push fs
	push ax
	push di

	mov ah, 4
	mul ah

	xor di, di
	mov fs, di
	mov di, ax

	cli

	mov ax, [es:si]
	mov [fs:di], ax
	call dnewline__
	call dprintnu16_ax_
	mov ax, [es:si + 2]
	mov [fs:di + 2], ax

	sti

	call dnewline__
	call dprintnu16_ax_

	call dnewline__

	pop di
	pop ax
	pop fs
	ret