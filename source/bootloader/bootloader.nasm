;Bootloader load kernel in memory and jmp to the same memory for execution
;	---------------
;	|0x0000:0x0000|
;	|     ...     |
;	|0x07c0:0x0000| <- bootloader [reg:offset, reg = cx, ds, es]
;	|     ...     |
;	|0x07c0:0x01fe| 0x55 -\  boot
;	|0x07c0:0x01ff| 0xaa -/  signature
;	|0x07e0:0x0000| <- ss:bp
;	|     ...     |
;	|0x07e0:0x1000| <- ss:sp [offer 4k for stack]
;	---------------

%define BOOTLOADER_ASM		0x0
%define KERNEL_SEGMENT		0x1000
%define KERNEL_OFFSET		0x0
%define SECTORS_FOR_READ	20

jmp 0x07c0:bootloader_start				; now, cs is 0x07c0

bootloader_start:
	mov ax, cs
	mov ds, ax
	mov es, ax

	cli
	add ax, 0x20						;	ax = 0x07e0
	mov ss, ax
	xor bp, bp
	mov sp, 0x1000
	sti

	cld

	mov [drive], dl

	mov si, boot_hello
	call string_print

	mov bx, KERNEL_SEGMENT
	mov es, bx
	mov bx, KERNEL_OFFSET
	mov al, SECTORS_FOR_READ			; sectors for read
	mov cl, 0x02						; from second sector
	call read_sectors

	jmp KERNEL_SEGMENT:KERNEL_OFFSET

;******************************************************************************
; Subroutines
;******************************************************************************

string_print:							;IN: si(pointer to zero termination string)
	push ax
	push si
	
	mov ah, 0x0e
	.again:
		lodsb
		cmp al, 0x0
		je ._again

		int 0x10
		jmp .again
	._again:
	pop si
	pop ax
	ret

read_sectors:
	mov ah, 0x02
	mov bx, 0x0
	mov ch, 0x0
	mov dh, 0x0
	mov dl, [drive]

	int 0x13
	jc .error

	ret
	.error:
		mov si, read_error
		call string_print
		jmp $

;******************************************************************************
; Variables
;******************************************************************************

	boot_hello: 	db 	'Hello. I am a bootloader.', 0
	read_error: 	db 	'Error on read sector from disk.', 0
	drive: 		db 	0

;******************************************************************************
; Boot signature
;******************************************************************************

times 510 - ($ - $$) db 0
dw 0xaa55
