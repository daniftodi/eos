all : image
	qemu image.img

image : bootloader kernel
	cat bootloader kernel > image.img
	rm bootloader kernel

bootloader : source/bootloader/bootloader.nasm
	nasm -f bin source/bootloader/bootloader.nasm -o bootloader

kernel : source/kernel/kernel.nasm
	nasm -f bin source/kernel/kernel.nasm -o kernel
